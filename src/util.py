import itertools


def find_subsets(_list, m):
    temp = list(itertools.combinations(_list, m))
    subsets = []
    for t in temp:
        subsets.append(list(t))
    return subsets
