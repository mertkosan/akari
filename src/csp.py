from constraint import *
from board import *


class Akari:
    def __init__(self, path):
        self.problem = Problem()

        self.board = read_board(path)
        self.board.calculate_domains()

        self.solutions = None

    def add_variables(self):
        domains = self.board.domains
        for variable in domains:
            domain = domains[variable][0]
            if domain is not None:
                self.problem.addVariable(variable, domain)

    def add_constraints(self):
        # rule1: no light flashes each other
        self.add_constraint_no_overlap()
        # rule2: black tile rule
        self.add_constraint_black_tile()
        # rule3: all bright tiles are flashed
        self.add_constraint_all_flashed()

    def add_constraint_no_overlap(self):
        bright_tiles = self.board.bright_tiles
        for tile1 in bright_tiles:
            for tile2 in bright_tiles:
                if tile1 != tile2:
                    self.problem.addConstraint(
                        lambda l1, l2, t1=tile1, t2=tile2:
                        self.no_overlap(t1, t2, l1, l2),
                        [tile1, tile2]
                    )

    def no_overlap(self, t1, t2, l1, l2):
        if len(l1) == 0:
            return True
        if t1 not in l2:
            return True
        return False

    def add_constraint_black_tile(self):
        black_tiles = self.board.black_tiles
        for tile in black_tiles:
            neighbours = self.board.find_neighbours(tile)
            for neighbour in neighbours:
                self.problem.addConstraint(lambda l1, b, position=neighbour, position_black=tile:
                                           self.black_tile_constraint(position, position_black, l1, b),
                                           [neighbour, tile])

    def black_tile_constraint(self, position, position_black, l1, b):
        neighbours = self.board.find_neighbours(position_black)
        if position in b:
            if len(l1) == 0:
                return False
        else:
            if len(l1) == 0:
                return True
            if position in neighbours:
                return False
        return True

    def add_constraint_all_flashed(self):
        bright_tiles = self.board.bright_tiles
        self.problem.addConstraint(lambda *args:
                                   self.all_bright(args),
                                   bright_tiles)

    def all_bright(self, *params):
        bright_tiles = self.board.bright_tiles
        params = params[0]
        tiles = []
        for tile in params:
            tiles += tile
        tiles = list(set(tiles))
        if sorted(tiles) == sorted(bright_tiles):
            return True
        return False

    def solve(self):
        self.solutions = self.problem.getSolutions()

    def print_solutions(self):
        for i, solution in enumerate(self.solutions):

            sys.stdout.write("========================\n")
            sys.stdout.write("Solution - {}\n".format(i+1))
            sys.stdout.write("========================\n")
            # prints domains for each key
            # keys = solution.keys()
            # keys = sorted(keys, key=lambda tup: tup[0])
            # keys = sorted(keys, key=lambda tup: tup[1])
            # for element in keys:
            #     print element, " -> ", solution[element]
            # print("==========================")

            board = self.board.board
            board = zip(*board)  # transpose
            domains = self.board.domains
            for y, k in enumerate(board):
                for x, j in enumerate(k):
                    element = domains[(x, y)][1]
                    value = solution.get((x, y), None)
                    if element == 'O' and value != []:
                        j = 'L'
                    sys.stdout.write("%s " % j)
                sys.stdout.write("\n")

            sys.stdout.write("========================\n")
            sys.stdout.flush()
