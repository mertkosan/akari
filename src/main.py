from csp import *


def main():
    path = "../res/problem/stage_hard_7x7.txt"

    akari = Akari(path)
    akari.board.print_initial_board()

    akari.add_variables()
    akari.add_constraints()
    akari.solve()
    akari.print_solutions()

if __name__ == '__main__':
    main()
