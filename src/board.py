import numpy as np
from util import *
import sys


class AkariBoard:
    def __init__(self, row, column):
        self.row = row
        self.column = column
        self.board = np.chararray(shape=(column, row))
        self.current_x = 0
        self.current_y = 0
        self.bright_tiles = []
        self.black_tiles = []
        self.domains = {}

    # fills the board
    def put_element(self, element):
        self.board[self.current_x, self.current_y] = element

        # fill the bright tiles and domains as None
        if element == 'O':
            self.bright_tiles.append((self.current_x, self.current_y))
        elif element == '#':
            pass
        else:
            self.black_tiles.append((self.current_x, self.current_y))

        self.domains[(self.current_x, self.current_y)] = (None, element)

        # pass to next slot
        self.current_x += 1
        if self.current_x == self.column:
            self.current_x = 0
            self.current_y += 1

    # this function calculates domains of variables and return it.
    def calculate_domains(self):
        for variable in self.domains:
            (_, element) = self.domains[variable]
            self.domains[variable] = (self.find_domain(variable), element)
        return self.domains

    def find_domain(self, variable):
        (_, element) = self.domains[variable]
        if element == 'O':
            domain = self.find_domain_white_tile(variable)
            domain = [domain, []]
        else:
            domain = self.find_domain_black_tile(variable, element)
        return domain

    def find_domain_white_tile(self, variable):
        domain = []
        i = variable[0]
        j = variable[1]
        temp_i = i
        while temp_i < self.column:
            if self.board[temp_i, j] == 'O':
                data = (temp_i, j)
                if data not in domain:
                    domain.append(data)
            else:
                break
            temp_i += 1
        temp_i = i
        while temp_i >= 0:
            if self.board[temp_i, j] == 'O':
                data = (temp_i, j)
                if data not in domain:
                    domain.append(data)
            else:
                break
            temp_i -= 1
        temp_j = j
        while temp_j < self.row:
            if self.board[i, temp_j] == 'O':
                data = (i, temp_j)
                if data not in domain:
                    domain.append(data)
            else:
                break
            temp_j += 1
        temp_j = j
        while temp_j >= 0:
            if self.board[i, temp_j] == 'O':
                data = (i, temp_j)
                if data not in domain:
                    domain.append(data)
            else:
                break
            temp_j -= 1
        return domain

    def find_domain_black_tile(self, variable, element):
        neighbours = self.find_neighbours(variable)
        if element == '#':
            domain = None
        elif element == '0':
            domain = [[]]
        else:
            domain = find_subsets(neighbours, int(element))
        return domain

    def find_neighbours(self, variable):
        i = variable[0]
        j = variable[1]
        il, iu, id, ir = i-1, i, i, i+1
        jl, ju, jd, jr = j, j-1, j+1, j
        neighbours = []
        if il >= 0 and self.domains[(il, jl)][1] == 'O':
            neighbours.append((il, jl))
        if ju >= 0 and self.domains[(iu, ju)][1] == 'O':
            neighbours.append((iu, ju))
        if jd < self.row and self.domains[(id, jd)][1] == 'O':
            neighbours.append((id, jd))
        if ir < self.column and self.domains[(ir, jr)][1] == 'O':
            neighbours.append((ir, jr))
        return neighbours

    def print_initial_board(self):
        sys.stdout.write("========================\n")
        sys.stdout.write("INITIAL BOARD\n")
        sys.stdout.write("========================\n")
        board = zip(*self.board)  # transpose
        for i in board:
            for j in i:
                sys.stdout.write("%s " % j)
            sys.stdout.write("\n")
        sys.stdout.write("========================\n")
        sys.stdout.flush()


# read board
def read_board(file_name):
    file_reader = open(file_name, "r")
    board_txt = file_reader.read()

    board_lines = board_txt.split("\n")
    row = len(board_lines)
    column = len(board_lines[0].split(" "))

    board = AkariBoard(row, column)
    for board_line in board_lines:
        words = board_line.split(" ")
        for word in words:
            board.put_element(word)

    return board
